package de.spigotplugins.playerhider.storage;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Pascal Falk on 22.11.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public final class Data {

    static {
        prefix = "§7[§ePlayerHider§7] ";

        ItemStack itemStack = new ItemStack(Material.BLAZE_ROD);
        ItemMeta im = itemStack.getItemMeta();
        im.setDisplayName("§aPlayerHider");
        itemStack.setItemMeta(im);

        playerHiderItem = itemStack;
    }

    public final static String prefix;
    public final static ItemStack playerHiderItem;

}
