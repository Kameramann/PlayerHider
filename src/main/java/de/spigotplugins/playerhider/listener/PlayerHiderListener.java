package de.spigotplugins.playerhider.listener;

import de.spigotplugins.playerhider.storage.Data;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Pascal Falk on 22.11.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class PlayerHiderListener implements Listener {

    private final ArrayList<Player> hiddenPlayers;
    private final Map<UUID, Long> cooldownMap;

    public PlayerHiderListener() {
        this.hiddenPlayers = new ArrayList<>();
        this.cooldownMap = new HashMap<>();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        for(Player hiders : hiddenPlayers){
            hiders.hidePlayer(e.getPlayer());
        }
        e.getPlayer().getInventory().setItem(4, Data.playerHiderItem);
    }


    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if(cooldownMap.containsKey(event.getPlayer().getUniqueId())) cooldownMap.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        Player player = e.getPlayer();

        if(!(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) return;
        if(e.getItem() == null) return;

        if(e.getItem().getType() == Material.BLAZE_ROD){
            if(e.getItem().equals(Data.playerHiderItem)) {

                if(cooldownMap.containsKey(player.getUniqueId())) {

                    if(cooldownMap.get(player.getUniqueId()) < System.currentTimeMillis()) {
                        toggleHide(player);
                        cooldownMap.put(player.getUniqueId(), System.currentTimeMillis() + 5000);
                    } else player.sendMessage(Data.prefix + "§cBitte warte noch einen Moment...");

                } else {

                    cooldownMap.put(player.getUniqueId(), System.currentTimeMillis() + 5000);
                    toggleHide(player);

                }

            }
        }
    }

    private void toggleHide(Player player) {
        if(hiddenPlayers.contains(player)){
            deactivateHide(player);
        } else {
            activateHide(player);
        }
    }

    private void deactivateHide(Player player) {
        hiddenPlayers.remove(player);
        for(Player all : Bukkit.getOnlinePlayers()){
            player.showPlayer(all);
        }
        player.sendMessage(Data.prefix + "§aDu siehst nun wieder alle Spieler!");
    }

    private void activateHide(Player player) {
        hiddenPlayers.add(player);
        for(Player all : Bukkit.getOnlinePlayers()){
            player.hidePlayer(all);
        }
        player.sendMessage(Data.prefix + "§aDu hast nun alle Spieler versteckt!");
    }

}


