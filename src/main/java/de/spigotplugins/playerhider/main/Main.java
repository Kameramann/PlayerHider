package de.spigotplugins.playerhider.main;

import de.spigotplugins.playerhider.listener.PlayerHiderListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

/**
 * Created by Pascal Falk on 22.11.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class Main extends JavaPlugin {

    @Getter
    private static Main instance;

    @Override
    public void onEnable() {
        instance = this;

        getLogger().log(Level.INFO, "§7Das PlayerHider §aPlugin §7wird gestartet §8» §bFree by WeLoveSpigotPlugins");
        Bukkit.getPluginManager().registerEvents(new PlayerHiderListener(), this);
    }

}
